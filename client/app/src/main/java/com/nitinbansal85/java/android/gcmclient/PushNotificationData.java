package com.nitinbansal85.java.android.gcmclient;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PushNotificationData extends BaseNotificationDataHolder {

    private JSONObject bigNotificationData = null;
    private BigPictureStyle bigPictureStyle = null;
    private BigTextStyle bigTextStyle = null;
    private InboxStyle inboxStyle = null;
    private String style = null;
    private String largeIcon = null;
    private boolean autoExpand = true;
    private List<ActionButton> actionButtons;
    private JSONObject cta;

    public PushNotificationData(){
        super();
    }

    public PushNotificationData(Context context, JSONObject json) throws JSONException {
        super(json, "dummy");
        this.context = context;
        this.largeIcon = json.isNull("image") ? null : json.optString("image");
        this.bigNotificationData = json.isNull("expandableDetails") ? null : json.optJSONObject("expandableDetails");
        if (this.bigNotificationData != null) {
            if (!bigNotificationData.isNull("style")) {
                this.style = bigNotificationData.optString("style");

                if (style.equalsIgnoreCase("BIG_TEXT")) {
                    bigTextStyle = new BigTextStyle(bigNotificationData);
                }
                else if (style.equalsIgnoreCase("BIG_PICTURE")) {
                    bigPictureStyle = new BigPictureStyle(bigNotificationData);
                }
                else  if (style.equalsIgnoreCase("INBOX")) {
                    inboxStyle = new InboxStyle(bigNotificationData);
                }

            }
            actionButtons = readActionButtons(bigNotificationData);

        }

    }

    private List<ActionButton> readActionButtons(JSONObject bigNotificationData) {
        List<ActionButton> actionButtons = new ArrayList<ActionButton>();
        cta = bigNotificationData.isNull("cta1") ? null : bigNotificationData.optJSONObject("cta1");
        if (cta != null) {
            if (!cta.isNull("id") && !cta.isNull("actionText") && !cta.isNull("actionLink")) {
                actionButtons.add(new ActionButton(cta.optString("id"), cta.optString("actionText"), cta.optString("actionLink")));
            } else {
                return null;
            }
        }
        cta = bigNotificationData.isNull("cta2") ? null : bigNotificationData.optJSONObject("cta2");
        if (cta != null) {
            if (!cta.isNull("id") && !cta.isNull("actionText") && !cta.isNull("actionLink")) {
                actionButtons.add(new ActionButton(cta.optString("id"), cta.optString("actionText"), cta.optString("actionLink")));
            } else {
                return null;
            }
        }
        cta = bigNotificationData.isNull("cta3") ? null : bigNotificationData.optJSONObject("cta3");
        if (cta != null) {
            if (!cta.isNull("id") && !cta.isNull("actionText") && !cta.isNull("actionLink")) {
                actionButtons.add(new ActionButton(cta.optString("id"), cta.optString("actionText"), cta.optString("actionLink")));
            } else {
                return null;
            }
        }
        return actionButtons;

    }

    public List<ActionButton> getActionButtons() {
        return this.actionButtons;
    }

    public ActionButton getActionButtonById(String id) {
        if (this.actionButtons != null && this.actionButtons.size() > 0) {
            for (ActionButton actionButton : actionButtons) {
                if (actionButton.getButtonId().equalsIgnoreCase(id)) {
                    return actionButton;
                }
            }
        }
        return null;
    }

    public boolean getAutoExpand() {
        return this.autoExpand;
    }

    public void setAutoExpand(boolean autoExpand) {
        this.autoExpand = autoExpand;
    }

    public boolean isBigNotification() {
        return this.bigNotificationData != null;
    }

    public String getLargeIcon() {
        return this.largeIcon;
    }

    public void setLargerIcon(String url) {
        this.largeIcon = url;
    }

    public AppEngageConstant.STYLE getStyle() {
        return AppEngageConstant.STYLE.valueOf(this.style);
    }

    public BigTextStyle getBigTextStyleData() {
        return this.bigTextStyle;
    }

    public BigPictureStyle getBigPictureStyleData() {
        return this.bigPictureStyle;
    }

    public InboxStyle getInboxStyleData() {
        return this.inboxStyle;
    }

    public class BaseStyleData {
        private String bigContentTitle = null;
        private String summary = null;

        public BaseStyleData(JSONObject json) {
            this.bigContentTitle = json.isNull("title") ? getNotificationTitle() : json.optString("title");
            this.summary = json.isNull("message") ? getNotificationContentText() : json.optString("message");

        }

        public void setBigContentTitle(String bigContentTitle) {
            this.bigContentTitle = bigContentTitle;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getBigContentTitle() {
            return this.bigContentTitle;
        }

        public String getSummary() {
            return this.summary;
        }
    }

    public class BigTextStyle extends BaseStyleData {
        private String bigText = null;

        public BigTextStyle(JSONObject json) {
            super(json);
            this.bigText = json.isNull("message") ? getNotificationContentText() : json.optString("message");

        }

        public void setBigText(String bigText) {
            this.bigText = bigText;
        }

        public String getBigText() {
            return this.bigText;
        }

    }

    public class BigPictureStyle extends BaseStyleData {
        private String bigPictureUrl = null;

        public BigPictureStyle(JSONObject json) {
            super(json);
            this.bigPictureUrl = json.isNull("image") ? null : json.optString("image");
        }

        public void setBigPictureUrl(String url) {
            this.bigPictureUrl = url;
        }

        public String getBigPictureUrl() {
            return this.bigPictureUrl;
        }

    }

    public class InboxStyle extends BaseStyleData {
        private List<String> lines = null;

        public InboxStyle(JSONObject json) {
            super(json);
            JSONArray inboxLines = json.isNull("lines") ? null : json.optJSONArray("lines");
            if (inboxLines != null) {
                lines = new LinkedList<String>();
                for (int i = 0; i < inboxLines.length(); i++) {
                    lines.add(inboxLines.optString(i));
                }
            }
        }

        public void setLines(List<String> lines) {
            this.lines = lines;
        }

        public List<String> getInboxLines() {
            return this.lines;
        }


    }

    public class ActionButton {

        private String id = null;
        private String text = null;
        private String ctaUrl = null;

        public ActionButton(String id, String text, String ctaUrl) {
            this.id = id;
            this.text = text;
            this.ctaUrl = ctaUrl;
        }

        public String getButtonId() {
            return this.id;
        }

        public String getButtonText() {
            return this.text;
        }

        public String getButtonActionUrl() {
            return this.ctaUrl;
        }

        public void setButtonText(String text) {
            this.text = text;
        }

        public void setButtonActionUrl(String ctaUrl) {
            this.ctaUrl = ctaUrl;
        }
    }


}
