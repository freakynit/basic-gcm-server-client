package com.nitinbansal85.java.android.gcmclient;

/**
 * Created by NitinBansal on 13/08/15.
 */
public class DeepLinkActionController {
    public static final String URI = "uri";

    public enum DEEPLINK_EVENTS {
        DEEPLINK_ACTIVITY_STARTED("start_activity"),
        DEEPLINK_URL_OPENED_IN_BROWSER("open_url_in_browser"),
        DEEPLINK_ANOTHER_APP_OPENED("start_another_app"),
        DEEPLINK_RICH_PAGE_OPENED("open_rich_page"),
        DEEPLINK_SHARE_DATA("share_data"),
        PUSH_NOTIFICATION_CANCELLED("push_notification_close");
        private String s;

        DEEPLINK_EVENTS(String s) {
            this.s = s;
        }

        public String getDeeplinkEventName() {
            return this.s;
        }

        public static DEEPLINK_EVENTS getDeeplinkEventNameByString(String event_name) {
            for (DEEPLINK_EVENTS v : DEEPLINK_EVENTS.values()) {
                if (v.getDeeplinkEventName().equalsIgnoreCase(event_name)) {
                    return v;
                }
            }

            return null;
        }
    }
}
