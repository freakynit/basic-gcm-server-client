package com.nitinbansal85.java.android.gcmclient.http;


import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class Response {

    private Exception exception = null;
    private Map<String, List<String>> responseHeaders = null;
    private boolean modifiedState = true;
    private InputStream inputStream = null;
    private int responseCode = -1;
    private String tag = "";
    private String cacheKey=null;

    public Response() {

    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Exception getException() {
        return this.exception;
    }

    public void setHeaders(Map<String, List<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return this.responseHeaders;
    }


    public void setModifiedState(boolean modifiedState) {
        this.modifiedState = modifiedState;
    }

    public boolean modified() {
        return this.modifiedState;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public InputStream getInputStream() {
        return this.inputStream;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return this.tag;
    }

    public void setCacheKey(String key){
        this.cacheKey=key;
    }
    public String getCacheKey(){
        return this.cacheKey;
    }
}
