package com.nitinbansal85.java.android.gcmclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nitinbansal85.java.android.gcmclient.http.NetworkObject;
import com.nitinbansal85.java.android.gcmclient.http.RequestMethod;
import com.nitinbansal85.java.android.gcmclient.http.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btnRegId;
    EditText etRegId;
    GoogleCloudMessaging gcm;
    String regid;
    String PROJECT_NUMBER = "936312110931";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRegId = (Button) findViewById(R.id.btnGetRegId);
        etRegId = (EditText) findViewById(R.id.etRegId);

        btnRegId.setOnClickListener(this);
    }
    public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(PROJECT_NUMBER);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return regid;
            }

            @Override
            protected void onPostExecute(final String regId) {
                etRegId.setText("Device registered, registration ID=" + regid + "\n");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NetworkObject networkObject;
                        Map<String,String> map=new HashMap<String, String>();
                        map.put("deviceToken", regId);

                        networkObject = new NetworkObject.Builder("http://ec2-54-188-179-125.us-west-2.compute.amazonaws.com:4000/updateDeviceToken", RequestMethod.POST, getApplicationContext(), null)
                                .setParams(AppEngageUtils.getParams(map))
                                .build();
                        Response response= networkObject.execute();
                        Log.v(this.getClass().getSimpleName(),response.getResponseCode()+"");
                    }
                }).start();
            }
        }.execute(null, null, null);
    }

    @Override
    public void onClick(View v) {
        getRegId();
    }
}
