//https://android.googleapis.com/gcm/send
//Authorization: key=AIzaSyDzuJxiGqFU23usgkMr7abR4WU6Fe43jTw
//Content-Type: application/json

var gcm = require('node-gcm');

// var message = new gcm.Message();

// message.addData('title', 'Test Title');
// message.addData('message', 'Test Message');

var regIds = ['APA91bHYH90wMF8XGIrgHerJ0xZMhX30kuJk7GDceLeWmsjIRma-xEk2i8GnBpTL180GMc_rjvs7Dnmc6wFOI8l1hau70HflRFsoI8yzTYQhvI7ZhxKtJbWNOC5Ur4YgUmVnoLXvVKomrOcQQfCx7uGBOVejqO0yzw'];

// Set up the sender with you API key
var sender = new gcm.Sender('AIzaSyDzuJxiGqFU23usgkMr7abR4WU6Fe43jTw');

//Now the sender can be used to send messages
var message = new gcm.Message({
	collapseKey: 'demo',
    priority: 3,
    contentAvailable: true,
    timeToLive: 3,
    delayWhileIdle: false,
    // data: {
    // 	"title": "title from data",
    // 	"message": "1 2 3 4 5 6 7 8 9 1 2 ❤ ❤ 1 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9"
    // }    
    data: {"data":{"notificationTemplate":{"identifier":"550251535512","title":"New Offers on Shirts","message":"Get upto 50% discount","image":"http://i-cdn.phonearena.com/images/article/49096-image/Nexus-5-unboxing.jpg","experimentId":"~1625fshdv","cta":{"id":"wqdfrw","type":"DEEP_LINK/EXTERNAL_URL/LANDING_PAGE","actionLink":"appengage://testapp.com/open_url_in_browser/http%3A%2F%2Fwww.webengage.com"},"custom":[{"key":"name","value":"shahrukh"},{"key":"age","value":23}],"expandableDetails":{"style":"BIG_TEXT","title":"Purchase new Polo T-Shirts","message":"At Flipkart, we believe that a wish is a desire that should be fulfilled for every Indian. And that is why you’ll see that there’s always something for you to shop for. Whether it’s an IIT-JEE entrance book or the latest Mi phone, you’ll find it here on Flipkart. Home to a huge collection of home furnishing products, appliances, footwear, television, clothes, mobiles, books, cameras and laptops, the list is as endless as the potential long line that you would end up in should you happen to brave the crowd at the end of your shopping trip at a mall","cta1":{"id":"id1","type":"DEEP_LINK/EXTERNAL_URL/LANDING_PAGE","actionLink":"appengage://testapp.com/start_activity/com.appengage.test.Activity2","actionText":"visit page"},"cta2":{"id":"id2","type":"DEEP_LINK/EXTERNAL_URL/LANDING_PAGE","actionLink":"appengage://testapp.com/start_activity/com.appengage.test.Activity3","actionText":"click now"}},"sampling":50,"tokens":[{"name":"lotus","value":"webengage","status":"ACTIVE"},{"name":"name","value":"","status":"ACTIVE"},{"name":"age","value":"","status":"ACTIVE"}]},"pushCredential":{"gcmServerKey":"AIzaSyA8xCSXkIdm_V_7iTKsMe_5M0zXPS9GIas","gcmProjectNumber":"299770535209"}},"registration_ids":["APA91bHYH90wMF8XGIrgHerJ0xZMhX30kuJk7GDceLeWmsjIRma-xEk2i8GnBpTL180GMc_rjvs7Dnmc6wFOI8l1hau70HflRFsoI8yzTYQhvI7ZhxKtJbWNOC5Ur4YgUmVnoLXvVKomrOcQQfCx7uGBOVejqO0yzw"]}
    // ,notification: {
    // 	title: "title from notification",
	   //  icon: 'ic_launcher',
	   //  body: "Body from notification"
    // }
});

// var message = new gcm.Message();
// message.addNotification({
//   	title: "title from notification",
// 	icon: 'ic_launcher',
// 	body: "Body from notification"
// });

sender.send(message, regIds, function (err, result) {
    if(err) console.error(err);
    else    console.log(result);
});

// sender.sendNoRetry(message, regIds, function (err, result) {
//     if(err) console.error(err);
//     else    console.log(result);
// });